sudo -u postgres psql

CREATE DATABASE tscapi;

\l -> listas bases de datos
\c tscapi ->seleccionar base de datos

CREATE TABLE IF NOT EXISTS users(
  id SERIAL PRIMARY KEY,
  name VARCHAR(40),
  email text
);

\dt -> listar tablas

INSERT INTO users (name, email) VALUES
  ('joe', 'joe@ibm.com'),
  ('ryan', 'ryan@ibm.com');
